# Gateware Builds Tester

## Description
Internal project intended for CI of the BeagleV-Fire gateware.

This script builds a set of bitstreams to test the ability to build gateware with different build option combinations (cape, MIPI connector, high-speed connector FPGA logic) and different bitstream component git branches.

## Installation
This script requires some Python packages to be installed:

```
pip3 install gitpython
pip3 install pyyaml
pip3 install junit_xml
```

## Usage
Usage:
```
python3 bitstream-builds-tester.py <TESTED_REPO_URL> <BITSTREAM_COMPONENT> <SOURCE_BRANCH> <DESTINATION_BRANCH>
```
Where:
- <TESTED_REPO_URL> is the URL of git repository containing the component under test.
- <BITSTREAM_COMPONENT> is used as part of CI to select which bitstream compoment is under test.  Allowed values are:
  - gateware
  - HSS
  - MSS
- <SOURCE_BRANCH> identifies the componet's branch under test. e.g. a feature branch or develop.
- <DESTINATION_BRANCH> identifies the intended merge branch target. e.g. develop, master

Example:
```
python3 bitstream-builds-tester.py https://git.beagleboard.org/beaglev-fire/gateware gateware feature-design-rename develop
```
