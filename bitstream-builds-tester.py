# SPDX-License-Identifier: MIT
import argparse
import os
import subprocess
import shutil
from junit_xml import TestSuite, TestCase
import yaml


def get_build_option_file_list(build_options_dir_name):
    build_options_path = os.path.join(os. getcwd(), "bitstream-builder/" + build_options_dir_name)
    build_option_files = []
    for file in os.listdir(build_options_path):
        if file.endswith('.yaml'):
            build_option_files.append(os.path.join(build_options_dir_name, file))
    return build_option_files


def adjust_ci_build_configs(tested_repo_url, bitstream_component, src_branch, dst_branch):
    print('  Bitstream component: ', bitstream_component)
    print('  Source branch: ', src_branch)
    print('  Destination branch: ', dst_branch)
    options_path = os.path.join(os.getcwd(), 'bitstream-builder', 'build-options')
    for cfg_file in os.listdir(options_path):
        yaml_file_name = os.path.join(options_path, cfg_file)
        print('    Build configuration: ', cfg_file)

        with open(yaml_file_name, 'r') as f:
            yaml_cfg = yaml.load(f, Loader=yaml.FullLoader)
            for component in yaml_cfg:
                if component == bitstream_component:
                    yaml_cfg[component]['branch'] = src_branch
                    yaml_cfg[component]['link'] = tested_repo_url
                else:
                    yaml_cfg[component]['branch'] = dst_branch
                    if component == 'HSS':
                        if dst_branch == 'develop':
                            yaml_cfg[component]['branch'] = 'develop-beaglev-fire'
                        elif dst_branch == 'main':
                            yaml_cfg[component]['branch'] = 'main-beaglev-fire'
                print('      ', component, ': ', yaml_cfg[component]['branch'])
                git_link = yaml_cfg[component]['link']

            with open(yaml_file_name, 'w') as f_out:
                yaml.dump(yaml_cfg, f_out)


def build_bitstreams(build_option_files):
    build_dir = "builds"
    if not os.path.exists(build_dir):
        os.makedirs(build_dir)
    os.chdir(build_dir)

    for build_option in build_option_files:
        option_build_dir_name = os.path.splitext(os.path.basename(build_option))[0]
        print("Build bistream configuration: ", option_build_dir_name)
        if not os.path.exists(option_build_dir_name):
            os.makedirs(option_build_dir_name)
        os.chdir(option_build_dir_name)
        cmd = "python3 ../../bitstream-builder/bvf-bitstream-builder.py ../../bitstream-builder/" \
              + build_option
        f = open("build_log.txt", "w")
        subprocess.call(cmd, shell=True, stdout=f)
        os.chdir('..')

    os.chdir('..')


def make_chdir(dir_name):
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
    os.chdir(dir_name)
    return os.getcwd()


def gather_artifacts(build_option_files):
    root_build_dir = os.path.join(os.getcwd(), "builds")
    root_artifacts_dir = make_chdir("artifacts")

    root_build_logs_dir = make_chdir("build_logs")
    os.chdir('..')

    root_bitstreams_dir = make_chdir("bitstreams")
    os.chdir('..')

    for build_option in build_option_files:
        option_build_dir_name = os.path.splitext(os.path.basename(build_option))[0]
        os.chdir(root_build_logs_dir)
        cwd = make_chdir(option_build_dir_name)
        src = os.path.join(root_build_dir, option_build_dir_name, "build_log.txt")
        dst = os.path.join(cwd, "build_log.txt")
        shutil.copy(src, dst)

        os.chdir(root_bitstreams_dir)
        cwd = make_chdir(option_build_dir_name)
        src = os.path.join(root_build_dir, option_build_dir_name, "bitstream")
        shutil.copytree(src, cwd, dirs_exist_ok=True)

        os.chdir('..')

    os.chdir('../..')


def get_hss_build_testcase(build_name, log):
    testcase = TestCase('HSS build', build_name, 123.345, '', '')
    if log.find('INFO: Successfully generated MSS component') == -1:
        testcase.add_error_info('MSS component not created.')
    if log.find('INFO  - mpfsBootmodeProgrammer completed successfully.') == -1:
        testcase.add_error_info('HSS executable not created.')
    return testcase


def get_synthesis_test_case(build_name, log):
    testcase = TestCase('Synthesis', build_name, 123.345, '', '')
    if log.find('Synthesis completed.') == -1:
        testcase.add_error_info('Synthesis failed.')
    return testcase


def get_place_route_test_case(build_name, log):
    testcase = TestCase('Place and route', build_name, 123.345, '', '')
    if log.find('Router completed successfully.') == -1:
        testcase.add_error_info('Place and route failed.')
    return testcase


def get_timing_analysis_test_case(build_name, log):
    testcase = TestCase('Timing analysis', build_name, 123.345, '', '')
    testcase.add_skipped_info('Timing analysis not enabled')
    return testcase


def get_bitstream_test_case(build_name, log):
    testcase = TestCase('Bitstream generation', build_name, 123.345, '', '')
    if log.find('Successfully generated bitstream file') == -1:
        testcase.add_error_info('Bitstream generation failed.')
    return testcase


def create_report(build_option_files):
    os.chdir("artifacts")
    os.chdir("build_logs")
    tss = []
    for build_option in build_option_files:
        build_name = os.path.splitext(os.path.basename(build_option))[0]
        with open(os.path.join(build_name, "build_log.txt"), 'rt') as f:
            log = f.read()
            test_cases = [get_hss_build_testcase(build_name, log), get_synthesis_test_case(build_name, log),
                          get_place_route_test_case(build_name, log), get_timing_analysis_test_case(build_name, log),
                          get_bitstream_test_case(build_name, log)]

            ts = TestSuite(build_name, test_cases)
            tss.append(ts)

    os.chdir('..')
    with open("tests_report.xml", "w") as f:
        TestSuite.to_file(f, tss, prettyprint=True)
    f.close()
    os.chdir('..')


def zip_artifacts():
    dir_name = os.path.join(os.getcwd(), "artifacts")
    shutil.make_archive("artifacts", 'zip', dir_name)


def parse_arguments():
    # Initialize parser
    parser = argparse.ArgumentParser()

    parser.add_argument('TestedRepoURL',
                        type=str,
                        help="URL of the git repository containing the component under test.",
                        action="store",
                        nargs='?')

    parser.add_argument('BitstreamComponent',
                        type=str,
                        help="Component for which CI is performed. Can be one of MSS, HSS, gateware.",
                        action="store",
                        nargs='?')
    parser.add_argument('SrcBranch',
                        type=str,
                        help="Source merge branch under test.",
                        action="store",
                        nargs='?')
    parser.add_argument('DstBranch',
                        type=str,
                        help="Destination merge branch under test",
                        action="store",
                        nargs='?')

    # Read argument(s) from command line
    args = parser.parse_args()
    tested_repo_url = args.TestedRepoURL
    if tested_repo_url is None:
        tested_repo_url = 'https://gitlab-ci-token:${CI_JOB_TOKEN}@git.beagleboard.org/beaglev-fire/gateware.git'
    bitstream_component = args.BitstreamComponent
    if bitstream_component is None:
        bitstream_component = 'gateware'
    src_branch = args.SrcBranch
    if src_branch is None:
        src_branch = 'develop'
    dst_branch = args.DstBranch
    if dst_branch is None:
        dst_branch = 'master'
#    build_options_dir_name = os.path.join("build-options", build_options_dir_name)
    build_options_dir_name = "build-options"
    return tested_repo_url, build_options_dir_name, bitstream_component, src_branch, dst_branch


def main():
    print("Test gateware builds.")
    tested_repo_url, build_options_dir_name, bitstream_component, src_branch, dst_branch = parse_arguments()

    build_option_files = get_build_option_file_list(build_options_dir_name)
    adjust_ci_build_configs(tested_repo_url, bitstream_component, src_branch, dst_branch)
    print("Build bitstreams.")
    build_bitstreams(build_option_files)
    print("Gather up artifacts.")
    gather_artifacts(build_option_files)
    print("Create build test report.")
    create_report(build_option_files)
    zip_artifacts()
    print("Gateware tests Complete")


if __name__ == '__main__':
    main()
